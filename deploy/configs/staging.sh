export REPLICAS=1
export URL="https://v2.postgres.ai"
export BASE_URL="/"
export SIGN_IN_URL="https://console-v2.postgres.ai/signin"
export BOT_WS_URL="wss://v2.postgres.ai/ai-bot-ws/"
export API_URL_PREFIX="https://v2.postgres.ai/api/general"