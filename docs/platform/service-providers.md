---
title: 3rd-party Data Subprocessors and Service Providers
sidebar_label: Postgres.ai data subprocessors
---
Effective date: 2025-02-05

Postgres.ai aims to be fully transparent regarding how your data is used, how it is collected, and with whom it can be shared.

Postgres.ai uses the following data suprocessors:

| Name of Subprocessor (with URL) | Location of Processing | Description |
| ------ | ------ | ------ |
| [Amazon Web Services (AWS)](https://aws.amazon.com/) | USA | Secondary data hosting (currently not used for Customer Data) |
| [GitLab](https://gitlab.com/) | USA | Development and project management tools (Git, CI/CD, issue tracker, etc.) |
| [GitHub](https://github.com/) | USA | Development and project management tools (Git, CI/CD, issue tracker, etc.) |
| [Google Analytics (GA)](https://analytics.google.com/analytics/web/) | USA | User behavior analytics |
| [Google Cloud Platform (GCP)](https://cloud.google.com/) | USA | Primary data hosting. Customer Data (including backups) are stored here |
| [Google Workspace](https://workspace.google.com/) | USA | Email and cloud office apps used by the Postgres.ai Team for business |
| [Hetzner](https://hetzner.com/) | USA, Germany | Cloud and dedicated hosting provider (currently not used for Customer Data) |
| [Intercom](https://www.intercom.com/) | USA | Customer support system |
| [Microsoft Azure](https://azure.microsoft.com/) | USA | Azure AI – access to LLM such as DeepSeek (deployed in US locations only) |
| [OpenAI](https://openai.com/) | USA | AI models provided via API (such as Whisper, GPT). *Customer Data is not used in OpenAI API calls, unless explicitly approved by Customer*
| [SendGrid](https://www.sendgrid.com/) | USA | Transactional mail services provider |
| [Slack](https://slack.com) | USA | Business communication platform |
| [Stripe](https://stripe.com) | USA | Payment provider |
| [Zoom](https://zoom.us/) | USA | Video Communications |

This list is well-maintained (should be there are any changes, the list will be edited).

User data can be shared with some of the Platform's service providers. These are companies who provide services on Postgres.ai's behalf, such as hosting our Services, marketing, advertising, social, analytics, support ticketing, credit card processing, security, and other similar services. These companies are subject to contractual requirements that govern the security and confidentiality of user data.

See also: [Privacy Policy](https://postgres.ai/privacy).
