---
title: Database Lab Engine administration
sidebar_label: Overview
slug: /how-to-guides/administration
description: How to administer Database Lab Engine
---

## Guides
- [Install DLE from Postgres.ai Console](/docs/how-to-guides/administration/install-dle-from-postgres-ai)
- [Install DLE from AWS Marketplace](/docs/how-to-guides/administration/install-dle-from-aws-marketplace)
- [Install DLE manually (Community Edition)](/docs/how-to-guides/administration/install-dle-manually)
- [How to configure PostgreSQL used by Database Lab Engine](/docs/how-to-guides/administration/postgresql-configuration)
- [How to manage Database Lab Engine](/docs/how-to-guides/administration/engine-manage)
- [How to manage Joe Bot](/docs/how-to-guides/administration/joe-manage)
- [Secure Database Lab Engine](/docs/how-to-guides/administration/engine-secure)
- [How to refresh data when working in the "logical" mode](/docs/how-to-guides/administration/logical-full-refresh)
- [Masking sensitive data in PostgreSQL logs when using CI Observer](/docs/how-to-guides/administration/ci-observer-postgres-log-masking)
- [Add disk space to ZFS pool without downtime](/docs/how-to-guides/administration/add-disk-space-to-zfs-pool)

