import React from 'react'

export const LoadingIcon = () => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
      <path
        stroke="var(--ifm-font-color-secondary)"
        strokeLinecap="round"
        strokeWidth="3.55556"
        d="M20.0001 12c0 1.3811-.3576 2.7386-1.0378 3.9405-.6803 1.2019-1.6601 2.2072-2.8441 2.9182-1.1841.7109-2.532 1.1032-3.9126 1.1387-1.3806.0354-2.74687-.2871-3.96585-.9362"
      />
    </svg>
  )
}