const resources = [
  {
    title: 'GitLab: How GitLab iterates on SQL performance optimization workflow to reduce downtime risks',
    description: 'Learn how SaaS company can improve database management processes.',
    preview: '/assets/thumbnails/case-study-gitlab.png',
    link: '/resources/case-studies/gitlab',
    internalLink: true,
    kind: 'Case study',
    date: '2021-06-18 20:00:00',
  },
];

export default resources;
