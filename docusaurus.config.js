const path = require('path')

const URL = !!process.env.URL ? process.env.URL : 'https://v2.postgres.ai/'
const COLOR_MODE = typeof window !== 'undefined' && window.localStorage.getItem('theme') === 'dark' ? 'dark' : 'light'
const API_URL_PREFIX = !!process.env.API_URL_PREFIX ? process.env.API_URL_PREFIX : 'https://postgres.ai/api/general' // was: 'https://v2.postgres.ai/api/general/'
const BASE_URL = !!process.env.BASE_URL ? process.env.BASE_URL : '/'
const REPOSITORY_URL = 'https://github.com/postgres-ai/database-lab-engine'
const SIGN_IN_URL = !!process.env.SIGN_IN_URL
  ? process.env.SIGN_IN_URL
  : '/signin'
const BOT_WS_URL = !!process.env.BOT_WS_URL ? process.env.BOT_WS_URL : '/ai-bot-ws/'

module.exports = {
  title:
    'Postgres.AI', // Title for your website.
  tagline:
    'Branching 🖖 and thin cloning ⚡️ for any Postgres database. Empower database testing in CI/CD. Optimize DB-related costs while improving time-to-market and software quality.',
  url: URL, // Your website URL.
  baseUrl: BASE_URL, // Base URL for your project.
  onBrokenLinks: 'log', //'throw',
  favicon: 'img/favicon.svg',
  organizationName: 'postgres-ai',
  projectName: 'docs',

  customFields: {
    signInUrl: SIGN_IN_URL,
    apiUrlPrefix: API_URL_PREFIX,
    botWSUrl: BOT_WS_URL
  },

  scripts: [
    BASE_URL + 'js/githubButton.js',
    { src: BASE_URL + 'js/cookieBanner.js?v3', async: true, defer: true },
  ],

  themes: ['@docusaurus/theme-mermaid'],

  themeConfig: {
    colorMode: {
      defaultMode: 'dark',
    },

    metadata: [
      // This options will overwrite options from pages with the same name/property.
      { 'http-equiv': 'x-dns-prefetch-control', content: 'on' },
      { property: 'og:locale', content: 'en_US' },
      { property: 'og:site_name', content: 'Postgres.AI' },
      { property: 'og:image', content: `${URL}/img/og-image-xl.png` },
      {
        property: 'article:publisher',
        content: 'https://www.linkedin.com/company/postgres-ai/',
      },
      { name: 'twitter:site', content: '@Database_Lab' },
      { name: 'twitter:creator', content: '@Database_Lab' },
      { name: 'twitter:image', content: `${URL}/img/og-image-xl.png` },
      { name: 'twitter:card', content: 'summary_large_image' }
    ],

    announcementBar: {
      id: 'advisory_group', // Any value that will identify this message to save the hidden status.
      content:
        "<a href='/blog/20240127-postges-ai-bot'>🚀 New Postgres.AI assistant: free GPT-4o for Postgres topics</a>",
      backgroundColor: '#D7EEF2',
      textColor: '#013A44',
      isCloseable: false,
    },

    navbar: {
      title: 'Postgres.AI',
      logo: {
        alt: 'Postgres.AI logo',
        src: 'img/logo.svg',
        width: '32px',
        height: '32px',
      },
      items: [
        {
          label: 'Products',
          position: 'right',
          to: '/',
          activeBaseRegex: '^/products',
          items: [
            {
              label: 'Postgres.AI Assistant',
              to: '/blog/20240127-postgres-ai-bot',
            },
            {
              label: 'DBLab Engine',
              to: '/products/dblab_engine'
            },
          ],
        },
        {
          to: '/pricing',
          label: 'DBLab Pricing',
          position: 'right',
        },
        {
          label: 'Docs',
          to: '/docs',
          activeBaseRegex: '^/docs',
          position: 'right',
          items: [
            {
              label: 'Documentation Home',
              to: '/docs',
              activeBaseRegex: '^/docs',
            },
            {
              label: 'DBLab for Amazon RDS',
              to: '/docs/tutorials/database-lab-tutorial-amazon-rds',
            },
            {
              label: 'DBLab HowTo guides',
              to: '/docs/how-to-guides',
            },
            {
              label: 'DBLab FAQ',
              to: '/docs/questions-and-answers',
            },
          ],
        },
        {
          to: '/blog',
          label: 'Blog',
          position: 'right',
        },
        {
          type: 'html',
          className: 'menu__list-item--sign-in-button',
          value: `<a href="/consulting" class="consulting-button">Consulting</a>`,
          position: 'right',
        },
        {
          type: 'html',
          className: 'menu__list-item--sign-in-button',
          value: `<a href=${SIGN_IN_URL} class="console-button">Console</a>`,
          position: 'right',
        },
      ],
    },

    algolia: {
      apiKey: 'b7b181027b0780f2526b7cdf86bb6d24',
      appId: 'X8XMQ9JWX7',
      indexName: 'postgres_algolia',
    },
    footer: {
      style: 'light',
      logo: {
        alt: 'Database Lab logo',
        src: 'img/logo.svg',
        width: '64px',
        height: '64px',
      },
      links: [
        {
          items: [
            {
              html: `
                <iframe 
                src=https://postgresai.instatus.com/embed-status/2c18fe48/${COLOR_MODE}-sm 
                width="230" 
                height="61" 
                frameBorder="0" 
                scrolling="no" 
                style="border: none;"
              >
              </iframe>            
                `,
            },
          ],
        },
        {
          title: 'Docs',
          items: [
            {
              label: 'FAQ',
              to: '/docs/questions-and-answers',
            },
            {
              label: 'Getting started',
              to: '/docs/',
            },
            {
              label: 'PostgresAI Assistant reference',
              to: '/docs/reference-guides/postgres-ai-bot-reference',
            },
            {
              label: 'DBLab guides',
              to: '/docs/how-to-guides',
            },
            {
              label: 'DBLab API',
              to: '/docs/reference-guides/database-lab-engine-api-reference',
            },
            {
              label: 'DBLab CLI',
              to: '/docs/reference-guides/dblab-client-cli-reference',
            },
            {
              label: 'DBLab configuration reference',
              to: '/docs/reference-guides/database-lab-engine-configuration-reference',
            },
          ],
        },
        {
          title: 'Products & Services',
          items: [
            {
              label: 'Consulting',
              to: '/consulting',
            },
            {
              label: 'PostgresAI assistant',
              to: '/blog/20240127-postgres-ai-bot',
            },
            {
              label: 'DBLab Engine',
              to: '/',
            },
            {
              label: 'Joe bot for SQL Optimization',
              to: '/products/joe',
            },
          ],
        },
        {
          title: 'Social',
          items: [
            {
              label: 'Community Slack',
              href: 'https://slack.postgres.ai/',
            },
            {
              label: 'Postgres.TV (YouTube)',
              href: 'https://www.youtube.com/PostgresTV',
            },
            {
              label: 'Postgres FM (podcast)',
              href: 'https://postgres.fm',
            },
            {
              label: 'GitLab',
              href: 'https://gitlab.com/postgres-ai',
            },
            {
              label: 'GitHub',
              href: 'https://github.com/postgres-ai',
            },
            {
              label: 'Twitter @postgres_ai',
              href: 'https://twitter.com/postgres_ai',
            },
            {
              label: 'Twitter @Database_Lab',
              href: 'https://twitter.com/Database_Lab',
            },
            {
              label: 'LinkedIn',
              href: 'https://www.linkedin.com/company/postgres-ai',
            },
          ],
        },
        {
          title: 'Links',
          items: [
            {
              label: 'Home',
              to: '/',
            },
            {
              label: 'Sign in',
              href: SIGN_IN_URL,
            },
            {
              label: 'Contact us',
              to: '/contact/',
            },
            {
              label: 'Documentation',
              to: '/docs/',
            },
            {
              label: 'Case studies',
              to: '/resources/',
            },
            {
              label: 'Terms of service',
              to: '/tos/',
            },
            {
              label: 'Privacy policy',
              to: '/privacy/',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Postgres.AI`,
    },

    prism: {
      theme: require('prism-react-renderer/themes/nightOwlLight'),
      darkTheme: require('prism-react-renderer/themes/nightOwl'),
    },
  },

  plugins: [
    [
      '@docusaurus/plugin-ideal-image',
      {
        quality: 70,
        max: 1030, // max resized image's size.
        min: 640, // min resized image's size. if original is lower, use that size.
        steps: 2, // the max number of images generated between min and max (inclusive)
      },
    ],
    [
      '@docusaurus/plugin-client-redirects',
      {
        redirects: [
          {
            from: ['/docs/oldDocPathFrom2019', '/docs/legacyDocPathFrom2016'], // string | string[]
            to: '/docs/roadmap' // string
          },
          {
            to: '/docs/reference-guides/database-lab-engine-api-reference',
            from: '/docs/database-lab/api-reference',
          },
          {
            from: '/docs/database-lab/cli-reference',
            to: '/docs/reference-guides/dblab-client-cli-reference'
          },
          {
            from: '/docs/database-lab/components',
            to: '/docs/reference-guides/database-lab-engine-components'
          },
          {
            from: '/docs/database-lab/config-reference',
            to: '/docs/reference-guides/database-lab-engine-configuration-reference'
          },
          {
            from: '/docs/joe-bot/config-reference',
            to: '/docs/reference-guides/joe-bot-configuration-reference'
          },
          {
            from: '/docs/joe-bot/commands-reference',
            to: '/docs/reference-guides/joe-bot-commands-reference'
          },
          {
            from: '/docs/db-migration-checker/config-reference',
            to: '/docs/reference-guides/db-migration-checker-configuration-reference'
          },
          {
            to: '/blog/20220106-explain-analyze-needs-buffers-to-improve-the-postgres-query-optimization-process',
            from: '/blog/20220106-explain-analyze-needs-buffers-to-improve-postgres-query-optimization process',
          },
          { to: '/docs/how-to-guides', from: '/docs/guides' },
          { from: '/docs/guides/administration', to: '/docs/how-to-guides/administration' },
          { from: '/docs/guides/data', to: '/docs/how-to-guides/administration/data' },
          { from: '/docs/guides/cli', to: '/docs/how-to-guides/cli' },
          { from: '/docs/guides/cloning', to: '/docs/how-to-guides/cloning' },
          { from: '/docs/guides/joe-bot', to: '/docs/how-to-guides/joe-bot' },
          { from: '/docs/guides/platform', to: '/docs/how-to-guides/platform' },
          { from: '/docs/tutorials/onboarding', to: '/docs/how-to-guides/platform/onboarding' },
          { from: '/support', to: '/contact/' },
          { from: '/careers/dba', to: '/careers/dbe' },
          {
            from: '/docs/how-to-guides/administration/machine-setup',
            to: '/docs/how-to-guides/administration/install-dle-manually' 
          },
          {
            from: '/blog/20240127-postges-ai-bot',
            to: '/blog/20240127-postgres-ai-bot' 
          },
          {
            from: '/docs/db-migration-checker',
            to: '/docs/database-lab/db-migration-checker'
          }
        ],
      },
    ],
    [
      path.resolve(__dirname, 'plugins/dynamic-routes'),
      {
        // this is the options object passed to the plugin
        routes: [
          {
            // using Route schema from react-router
            path: '/universe',
            exact: false, // this is needed for sub-routes to match!
            component: path.resolve(__dirname, 'src/dynamicPages/universe'),
          },
          {
            path: '/chats/:chatId',
            exact: false,
            component: path.resolve(__dirname, 'src/dynamicPages/chats'),
          }
        ],
      },
    ],
    [
      path.resolve(__dirname, 'plugins/docusaurus-plugin-google-gtm'),
      {
        trackingID: 'G-SM4CXEQJYY',
      },
    ],
    require.resolve('./plugins/route-change'),
  ],

  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl: 'https://gitlab.com/postgres-ai/docs/-/edit/master/',
          routeBasePath: '/docs/',
        },
        blog: {
          showReadingTime: true,
          editUrl: 'https://gitlab.com/postgres-ai/docs/-/edit/master/',
          path: 'blog',
          routeBasePath: 'blog',
          postsPerPage: 10,
          blogSidebarCount: 0, // 0 to disable the sidebar with a list of posts.
          feedOptions: {
            type: 'all',
            title: '', // default to siteConfig.title
            description: '', // default to  `${siteConfig.title} Blog`
            copyright: 'Postgres.AI',
            language: undefined, // possible values: http://www.w3.org/TR/REC-html40/struct/dirlang.html#langcodes
          },
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
        pages: {
          exclude: [
            '**/_*.{js,jsx,ts,tsx,md,mdx}',
            '**/*.test.{js,ts}',
            '**/__tests__/**',
            '**/console',
          ],
        },
      },
    ],
  ],
}
